require asyn
require s7plc
require calc
require modbus

# @field IPADDR
# @type STRING
# PLC IP address

# @field RECVTIMEOUT
# @type INTEGER
# PLC->EPICS receive timeout (ms), should be longer than frequency of PLC SND block trigger (REQ input)

# @field rfq-010_ctrl-plc-001_VERSION
# @runtime YES


# S7 port           : 2000
# Input block size  : 214 bytes
# Output block size : 0 bytes
# Endianness        : BigEndian
s7plcConfigure("RFQ-010:Ctrl-PLC-001", $(IPADDR), 2000, 214, 0, 1, $(RECVTIMEOUT), 0)

# Modbus port       : 502
drvAsynIPPortConfigure("RFQ-010:Ctrl-PLC-001", $(IPADDR):502, 0, 0, 1)

# Link type         : TCP/IP (0)
modbusInterposeConfig("RFQ-010:Ctrl-PLC-001", 0, $(RECVTIMEOUT), 0)

# Slave address     : 0
# Function code     : 16 - Write Multiple Registers
# Addressing        : Absolute (-1)
# Data segment      : 2 words
drvModbusAsynConfigure("RFQ-010:Ctrl-PLC-001write", "RFQ-010:Ctrl-PLC-001", 0, 16, -1, 2, 0, 0, "S7-1500")

# Load plc interface database
dbLoadRecords("$(E3_IOCSH_TOP)/db/rfq-010_ctrl-plc-001.db", "PLCNAME=RFQ-010:Ctrl-PLC-001, S7_PORT=2000, MODBUS_PORT=502")

var asCheckClientIP 1
asSetFilename("$(E3_IOCSH_TOP)/ncl.acf")
